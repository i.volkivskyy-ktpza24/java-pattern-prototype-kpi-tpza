public class CarsDemo {

    public static void main(String[] args) {
        Cars prot = new Cars("BMW", "X6", 200000, 2020);
        System.out.println(prot);

        Cars clone1 = (Cars) prot.createClone();
        Cars clone2 = (Cars) prot.createClone();
        Cars clone3 = (Cars) prot.createClone();

        clone1.setBrand("Mersedes");
        clone1.setModel("C63 AMG");
        clone1.setPrice(3000000);
        clone1.setYear(2018);

        clone2.setBrand("Mersedes");
        clone2.setModel("AMG GT-R");
        clone2.setPrice(5880832);
        clone2.setYear(2021);

        clone3.setBrand("Devil Sixteen");
        clone3.setModel("Gypercar");
        clone3.setPrice(500000000);
        clone3.setYear(2020);

        System.out.println(clone1);
        System.out.println(clone2);
        System.out.println(clone3);


    }
}
